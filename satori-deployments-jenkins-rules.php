<?php
/**
 * Satori deployments Jenkins Define rules
 *
 * Defined rules used throughout the plugin operations
 *
 */

if ( !defined( 'ABSPATH' ) ) {
    die();
}

if ( !defined( 'SATJENKINS_FILE' ) )
define( 'SATJENKINS_FILE', 'satori-deployments-jenkins/satori-deployments-jenkins.php' );

if ( !defined( 'SATJENKINS_DIR' ) )
    define( 'SATJENKINS_DIR', realpath( dirname( __FILE__ ) ) );

if ( !defined( 'SATJENKINS_TABLE' ) )
	define( 'SATJENKINS_TABLE', 'jenkins_deployments_activity_log' );

if ( !defined( 'SATJENKINS_DB_VERSION' ) )
	define( 'SATJENKINS_DB_VERSION', '1.2' );
