<?php
/**
 * Satori deployments Enqueuing Files
 *
 * Function to load styles and front end scripts
 *
 */

if ( !defined( 'ABSPATH' ) ) {
    die();
}

// Register admin scripts for custom fields
function load_satori_deployments_jenkins_wp_admin_style() {
        // admin always last
        wp_enqueue_style( 'satori_deployments_jenkins_css', plugin_dir_url(dirname(__FILE__)) . 'css/satori_deployments_jenkins_admin.css', array(), '1.1.5' );
        wp_enqueue_script( 'satori_deployments_jenkins_script', plugin_dir_url(dirname(__FILE__)) . 'js/satori_deployments_jenkins_admin.js', array(), '1.1.2' );
        wp_localize_script( 'satori_deployments_jenkins_script', 'the_ajax_script', array( 
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'nonce' => wp_create_nonce( "satori_deployments_jenkins_response_nonce"),
        ));  
}
add_action( 'admin_enqueue_scripts', 'load_satori_deployments_jenkins_wp_admin_style' );
