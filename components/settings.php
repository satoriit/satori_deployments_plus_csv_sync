<?php

if ( !defined( 'ABSPATH' ) ) {
    die();
}

add_action('admin_head', 'satori_deployments_jenkins_custom_favicon');
function satori_deployments_jenkins_custom_favicon() {
  echo '
    <style>
    .dashicons-satori-deployments {
        background-image: url("'. plugin_dir_url(dirname(__FILE__)) .'/img/satori.png");
        background-repeat: no-repeat;
        background-position: center;
    }
    </style>
  ';
}

// Create activity log table
add_action( 'init', 'satori_deployments_jenkins_register_activity_log_table', 1 );
add_action( 'switch_blog', 'satori_deployments_jenkins_register_activity_log_table' );

function satori_deployments_jenkins_register_activity_log_table() {
  $installed_ver = get_option( "satori_deployments_jenkins_db_version" );
  if ( !$installed_ver || $installed_ver != SATJENKINS_DB_VERSION ) {
    global $wpdb;

    $wpdb->SATJENKINS_TABLE = $wpdb->prefix . SATJENKINS_TABLE;
  	$sql_create_table = "CREATE TABLE {$wpdb->SATJENKINS_TABLE} (
            log_id bigint(20) unsigned NOT NULL auto_increment,
            user_name varchar(60) NOT NULL default '0',
            activity varchar(255) NOT NULL default 'updated',
            activity_date TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            job_status varchar(25) NOT NULL default '',
            environment varchar(25) NOT NULL default '',
            job_number varchar(25),
            queue_url varchar(255),
            PRIMARY KEY  (log_id),
            KEY user_id (user_name)
       ) $charset_collate; ";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
  	dbDelta( $sql_create_table );
    update_option("satori_deployments_jenkins_db_version", SATJENKINS_DB_VERSION);
  }
}

// Trigger create tables on plugin activation
function satori_deployments_jenkins_create_tables() {
  // Code for creating a table goes here
  require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	global $wpdb;
	global $charset_collate;
	// Call this manually as we may have missed the init hook
	satori_deployments_jenkins_register_activity_log_table();
}
// Create tables on plugin activation
register_activation_hook( __FILE__, 'satori_deployments_jenkins_create_tables' );

// Trigger create table check in case the db version changes
function satori_deployments_jenkins_update_db_check() {
    if ( get_site_option( "satori_deployments_jenkins_db_version" ) != SATJENKINS_DB_VERSION ) {
        satori_deployments_jenkins_register_activity_log_table();
    }
}
add_action( 'plugins_loaded', 'satori_deployments_jenkins_update_db_check' );


// create custom plugin settings menu
add_action('admin_menu', 'satori_deployments_jenkins_create_menu');
function satori_deployments_jenkins_create_menu() {
        //create new top-level menu
        if ( empty ( $GLOBALS['admin_page_hooks']['satori-deployments-settings'] ) ) {
                add_menu_page('DeploymentsSettings', 'Deployments', 'editor', 'satori-deployments-settings', 'satori_deployments_main_page' , 'dashicons-building' );
        }
        add_submenu_page('satori-deployments-settings',
            'Site Deployments',
            'Site Deployments',
            'publish_pages',
            __FILE__.'/custom',
            'satori_deployments_jenkins_settings_page'
        );
        //call register settings function
        add_action( 'admin_init', 'register_satori_deployments_jenkins_settings' );
}

// Register admin settings
function register_satori_deployments_jenkins_settings() {
    //register our settings
    register_setting( 'satori-deployments-jenkins-settings-group', 'satori_deployments_jenkins_url', 'satori_deployments_jenkins_url_validate' );
    register_setting( 'satori-deployments-jenkins-settings-group', 'satori_deployments_jenkins_staging_url', 'satori_deployments_jenkins_url_validate' );
    register_setting( 'satori-deployments-jenkins-settings-group', 'satori_deployments_jenkins_user', 'satori_deployments_jenkins_user_validate' );
    register_setting( 'satori-deployments-jenkins-settings-group', 'satori_deployments_jenkins_api', 'satori_deployments_jenkins_api_validate' );
    register_setting( 'satori-deployments-jenkins-settings-group', 'satori_deployments_jenkins_db_version', 'satori_deployments_jenkins_db_version' );
}

// Uninstall hook
function satori_deployments_jenkins_uninstall_hook() {
  // Delete setting values
  delete_option('satori_deployments_jenkins_url');
  delete_option('satori_deployments_jenkins_user');
  delete_option('satori_deployments_jenkins_api');

  // Clear Cron tasks
  wp_unschedule_hook( 'satori_deployments_jenkins_schedule_poll' );

  // Delete custom table
  global $wpdb;
  $table_name = $wpdb->prefix . SATJENKINS_TABLE;
  $sql = "DROP TABLE IF EXISTS $table_name";
  $wpdb->query($sql);
  delete_option('satori_deployments_jenkins_db_version');

}
register_uninstall_hook( SATJENKINS_FILE, 'satori_deployments_jenkins_uninstall_hook' );

// Deactivation hook
function satori_deployments_jenkins_deactivation() {
  // Clear Cron tasks
  wp_unschedule_hook( 'satori_deployments_jenkins_schedule_poll' );
}
register_deactivation_hook( SATJENKINS_FILE, 'satori_deployments_jenkins_deactivation' );

// Validate Input for Admin options
function satori_deployments_jenkins_url_validate($data){
	if(filter_var($data, FILTER_VALIDATE_URL,FILTER_FLAG_QUERY_REQUIRED)) {
   		return $data;
   	} else {
   		add_settings_error(
            'satori_deployments_jenkins_url',
            'satori-deployments-jenkins-notice',
            'You did not enter a valid URL for the Jenkins push',
            'error');
   	}
}

function satori_deployments_jenkins_user_validate($data){
	if(filter_var($data, FILTER_SANITIZE_STRING)) {
   		return $data;
   	} else {
   		add_settings_error(
            'satori_deployments_jenkins_user',
            'satori-deployments-jenkins-notice',
            'You did not enter a valid string for the username field',
            'error');
   	}
}

function satori_deployments_jenkins_api_validate($data){
	if(filter_var($data, FILTER_SANITIZE_STRING)) {
   		return $data;
   	} else {
   		add_settings_error(
            'satori_deployments_jenkins_api',
            'satori-deployments-jenkins-notice',
            'You did not enter a valid string for the API field',
            'error');
   	}
}

// Validate admin options
function satori_deployments_jenkins_check_options() {
    // If enabled is not set
    if(empty(esc_attr(get_option('satori_deployments_jenkins_url') ))) return false;
    if(empty(esc_attr(get_option('satori_deployments_jenkins_staging_url') ))) return false;
    if(empty(esc_attr(get_option('satori_deployments_jenkins_api') ))) return false;
    if(empty(esc_attr(get_option('satori_deployments_jenkins_user') ))) return false;

    return true;

}

// Force cron schedule change if detected
function satori_deployments_jenkins_cron_validate($data){
  $cron_schedule = esc_attr($data);
  if (get_transient(S8JENKINS_CRON_SCHEDULE) && get_transient(S8JENKINS_CRON_SCHEDULE) === $cron_schedule) {
    set_transient(S8JENKINS_CRON_SCHEDULE, $cron_schedule, 0);
    return $cron_schedule;
  } else {
    set_transient(S8JENKINS_CRON_SCHEDULE, $cron_schedule, 0);
    wp_clear_scheduled_hook( 'satori_deployments_jenkins_cron_hook' );
    return $cron_schedule;
  }
}
