<?php

if ( !defined( 'ABSPATH' ) ) {
    die();
}

use Carbon\Carbon;
use Carbon\CarbonTimeZone;
require 'constants.php';


// Handle the ajax triggers
add_action( 'wp_ajax_satori_deployments_jenkins_staging_push', 'satori_deployments_jenkins_push' );
add_action( 'wp_ajax_satori_deployments_jenkins_push', 'satori_deployments_jenkins_push' );
function satori_deployments_jenkins_push() {
    if (current_user_can('administrator') && satori_deployments_jenkins_check_options()) {

        $user_pushed = wp_get_current_user();
        $user_pushed->env = $_GET['action'] == 'satori_deployments_jenkins_push' ? "prod" : "staging";
    if ( wp_verify_nonce($_GET['_wpnonce'], 'process')) {
        satori_deployments_jenkins_poll($user_pushed);
    } else if ( wp_verify_nonce($_GET['_wpnonce'], 'process') && $_GET['schedule'] !== 'immediate') {
            switch ($_GET['schedule']) {
                case 'tonight':
                    satori_deployments_jenkins_schedule_push(Carbon::now()->setTime(23,30,0)->timestamp, $user_pushed);
                    break;
                case 'tomorrow_morning':
                    satori_deployments_jenkins_schedule_push(Carbon::now()->setTime(7,30,0)->add(1, 'day')->timestamp, $user_pushed);
                    break;
                case 'tomorrow_evening':
                    satori_deployments_jenkins_schedule_push(Carbon::now()->setTime(23,30,0)->add(1, 'day')->timestamp, $user_pushed);
                    break;
            }
        die();
    } else {
            die();
        }
    } else {
        die();
    }
}

// Handle the actual jenkins GET
function satori_deployments_jenkins_poll($user_pushed) {
    $url_to_call = get_env_url($user_pushed->env);
    $next_build_number = get_next_build_number($user_pushed->env);
    $response = call_jenkins_api($url_to_call);
    if (is_array($response) && $response['response']['code'] == '201') {
        $headers = wp_remote_retrieve_headers($response);
        $date = date('Y-m-d H:i:s');
        echo $date . ' / ' . $user_pushed->user_login . ' : Pushed to ' . $user_pushed->env;
        satori_deployments_jenkins_activity_log(
            $user_pushed->user_login,
            'pushed to ' . $user_pushed->env,
            $user_pushed->env,
            $next_build_number,
            Constants::JOB_IN_PROGRESS,
            $headers['location']
        );
    } else {
        echo 'error_detected : ';
        if (is_array($response['response'])) {
            echo $response['response']['code'] . ' - ' . $response['response']['message'];
        } else {
            echo 'unknown';
        }
    }
}

/**
 * @param $user_pushed
 * @return array|WP_Error
 */
function call_jenkins_api($url_to_call): array
{
    $jenkins_user = esc_attr(get_option('satori_deployments_jenkins_user'));
    $jenkins_api = esc_attr(get_option('satori_deployments_jenkins_api'));
    // Set headers for WP Remote get
    $headers = array(
        'Content-type: application/json',
        'Authorization' => 'Basic ' . base64_encode($jenkins_user . ':' . $jenkins_api),
    );

    // Use WP Remote Get to call jenkins
    return wp_remote_get(
        esc_attr($url_to_call),
        array(
            'headers' => $headers,
            'httpversion' => '1.1',
            'timeout' => '10',
        )
    );
}

function get_env_url($env)
{
    if ($env == 'staging') {
        return get_option('satori_deployments_jenkins_staging_url');
    } else if($env == 'prod') {
        return get_option('satori_deployments_jenkins_url');
    }
    save_error('get env url returned null, env was set to ' . $env, $env);
    return null;
}

/**
 * @param $error_message
 * @param $env
 * @return void
 */
function save_error($error_message, $env): void
{
    satori_deployments_jenkins_activity_log(
        "system",
        $error_message,
        $env,
        0,
        Constants::ERROR_OCCURRED,
        false
    );
}

// Setup the action that may be used as a one-off scheduled job
add_action( 'satori_deployments_jenkins_schedule_poll', 'satori_deployments_jenkins_poll', 10, 1 );

// Get the log entriees
function satori_deployments_jenkins_get_activity_log() {
    if (current_user_can('administrator')) {
        global $wpdb;
        $table_name = $wpdb->prefix . SATJENKINS_TABLE;
        $activity_log_array = $wpdb->get_results("SELECT * FROM $table_name ORDER BY activity_date DESC");
        foreach ($activity_log_array as $activity_log) {
            if ($activity_log->job_status == Constants::JOB_IN_PROGRESS) {
                if (is_build_in_progress($activity_log->queue_url)) {
                    continue;
                } else {
                    if (!$activity_log->environment || $activity_log->job_number == null) {
                        echo "unable to fetch job status for build that happened at: " . $activity_log->activity_date;
                    } else {
                        $job_status = get_job_status($activity_log->environment, $activity_log->job_number);
                        $activity_log->job_status = $job_status;
                        if ($job_status == Constants::JOB_NO_RESULT_YET) {
                            continue;
                        }
                        $wpdb->update(
                            $table_name,
                            array('job_status' => $job_status),
                            array('log_id' => $activity_log->log_id)
                        );
                    }
                }
            }
        }
        return $activity_log_array;
    } else {
        return null;
    }
}

/**
 * @param $result
 * @return string
 */
function retrieve_job_status_from_api_response($result): string
{
    switch ($result) {
        case 'SUCCESS':
            return Constants::JOB_SUCCESSFUL;
        case 'FAILURE':
            return Constants::JOB_FAILED;
        case 'ABORTED':
            return Constants::JOB_ABORTED;
        default:
            return "UNHANDLED: " . $result;
    }
}

/**
 * @param $env
 * @param $build_number
 * @return string
 */
function get_job_status($env, $build_number): string
{
    $url = get_env_url($env);
    $matches = array();
    if (preg_match('/(.+)build\?/', $url, $matches)) {
        $response = call_jenkins_api($matches[1] . $build_number . '/api/json');
        if (is_array($response) && $response['response']['code'] == '200') {
            $json_response = json_decode($response['body']);
            if (is_object($json_response)) {
                if ($json_response->result != null) {
                    return retrieve_job_status_from_api_response($json_response->result);
                } else {
                    return Constants::JOB_NO_RESULT_YET;
                }
            } else {
                echo "could not find the build status for " . $env . " build #" . $build_number;
            }
        }
    }
    return Constants::JOB_NO_RESULT_YET;
}

function get_next_build_number($env) {
    $url = get_env_url($env);
    $matches = array();
    if (preg_match('/(.+)build\?/', $url, $matches)) {
        $response = call_jenkins_api($matches[1] . 'api/json');
        if (is_array($response) && $response['response']['code'] == '200') {
            $json_response = json_decode( $response['body'] );
            if (is_object($json_response) && $json_response->nextBuildNumber != null) {
                return $json_response->nextBuildNumber;
            } else {
                echo "could not find the next build number for " . $env;
            }
        } else {
            echo "failed to get meaningful response, got an error instead";
            var_dump($response);
        }
    } else {
        echo "didn't find a match in the URL when trying to get next build number";
        return null;
    }
}

function is_build_in_progress($url) {
    $response = call_jenkins_api($url);
    if (is_array($response) && $response['response']['code'] == 404) {
        return false;
    } else {
        return true;
    }
}

// One time schedule action for jenkins poll
function satori_deployments_jenkins_schedule_push($schedule, $user_pushed) {
    if (current_user_can('administrator') && satori_deployments_jenkins_check_options() && $schedule) {
        $schedule_human = Carbon::createFromTimestamp($schedule)->toDateTimeString();

        // Set the cron schedule
        satori_deployments_jenkins_activity_log(
            $user_pushed->user_login,
            'scheduled push for ' . $schedule_human,
            $user_pushed->env,
            0,
            Constants::JOB_SCHEDULED,
            false
        );
        if ( ! wp_next_scheduled( 'satori_deployments_jenkins_schedule_poll', array ( $user_pushed ) ) ) {
            wp_schedule_single_event( $schedule, 'satori_deployments_jenkins_schedule_poll', array($user_pushed));
        } else {
            satori_deployments_jenkins_activity_log(
                $user_pushed->user_login,
                'cleaning up previously scheduled push & scheduling new one for ' .$schedule_human,
                $user_pushed->env,
                0,
                Constants::JOB_RESCHEDULED,
                false
            );
            wp_unschedule_hook( 'satori_deployments_jenkins_schedule_poll' );
            wp_schedule_single_event( $schedule, 'satori_deployments_jenkins_schedule_poll', array($user_pushed));
        }
        // Display notice and log activity
        echo 'Schedule initiated to push on ' . $schedule_human;
    } else {
        echo 'An unknown error occurred while scheduling the push.';
    }
}

// Function to handle activity logging
/**
 * @param $log_user
 * @param $log_activity_text
 * @param $env
 * @param $job_number
 * @param $job_status
 * @param $queue_url
 * @return void
 */
function satori_deployments_jenkins_activity_log($log_user, $log_activity_text, $env, $job_number, $job_status, $queue_url) {
    $log_date = Carbon::now()->format('Y-m-d H:i:s');
    $save_job_number = is_int($job_number) ? $job_number : null;
    $queue_url = $queue_url ? $queue_url : null;
    $insert_data = array(
        'user_name' => sanitize_text_field($log_user),
        'activity' => sanitize_text_field($log_activity_text),
        'environment' => sanitize_text_field($env),
        'activity_date' => $log_date,
        'job_status' => sanitize_text_field($job_status),
        'job_number' => $save_job_number,
        'queue_url' => sanitize_text_field($queue_url),
    );
    global $wpdb;
    $wpdb->insert(
        $wpdb->prefix . SATJENKINS_TABLE,
        $insert_data
    );
}
