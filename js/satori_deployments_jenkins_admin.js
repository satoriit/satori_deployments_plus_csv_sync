jQuery(document).ready(function() {
	jQuery('#satori-deployments-jenkins-staging-push-schedule').change(function() {
		var satori_deployments_jenkins_staging_item=jQuery(this);
		var satori_deployments_jenkins_staging_href = jQuery('#satori-deployments-jenkins-staging-push').attr('href');
		var satori_deployments_jenkins_staging_pushurl = new URL(satori_deployments_jenkins_staging_href);
		satori_deployments_jenkins_staging_pushurl.searchParams.set("schedule", satori_deployments_jenkins_staging_item.val());
		jQuery('#satori-deployments-jenkins-push').attr('href', satori_deployments_jenkins_staging_pushurl);
	});
	jQuery('#satori-deployments-jenkins-push-schedule').change(function() {
    	var satori_deployments_jenkins_item=jQuery(this);
    	var satori_deployments_jenkins_href = jQuery('#satori-deployments-jenkins-push').attr('href');
    	var satori_deployments_jenkins_pushurl = new URL(satori_deployments_jenkins_href);
    	satori_deployments_jenkins_pushurl.searchParams.set("schedule", satori_deployments_jenkins_item.val());
    	jQuery('#satori-deployments-jenkins-push').attr('href', satori_deployments_jenkins_pushurl);
    });

	jQuery(document).on( 'click', '#satori-deployments-jenkins-push', function(e) {
		e.preventDefault();
		//if (confirm('Are you sure you want to push staging to production?')) {
			var button = jQuery(this);
	    	var url = button.attr('href');
	    	jQuery.ajax({
	        	url: url,
	        	data: {
	            	'action': 'satori_deployments_jenkins_push',
	        	},
	        	success:function(data) {
	            	// This outputs the result of the ajax request
	            	jQuery('.satori-deployments-jenkins-push-progress').html(data);
	        	},
	        	error: function(errorThrown){
	            	console.log('Error : ' + JSON.stringify(errorThrown));
	        	}
			});
		//}
	});

	jQuery(document).on( 'click', '#satori-deployments-jenkins-staging-push', function(e) {
		e.preventDefault();
		//if (confirm('Are you sure you want to push staging to production?')) {
			var button = jQuery(this);
	    	var url = button.attr('href');
	    	jQuery.ajax({
	        	url: url,
	        	data: {
	            	'action': 'satori_deployments_jenkins_staging_push',
	        	},
	        	success:function(data) {
	            	// This outputs the result of the ajax request
	            	jQuery('.satori-deployments-jenkins-push-staging-progress').html(data);
	        	},
	        	error: function(errorThrown){
	            	console.log('Error : ' + JSON.stringify(errorThrown));
	        	}
			});
		//}
	});
});
