<?php
/**
 * Plugin Name: Satori Deployments Jenkins Integration
 * Plugin URI: https://satoriit.co.uk
 * Description: Plugin for deploying sites triggering Jenkins jobs
 * Version: 1.0.0
 * Author: Satori
 * Author URI: https://satoriit.co.uk
 * License: GPLv3
 */

// Composer dependencies
if ( is_readable( __DIR__ . '/vendor/autoload.php' ) ) {
    require __DIR__ . '/vendor/autoload.php';
}

require_once(plugin_dir_path(__FILE__).'satori-deployments-jenkins-rules.php' );
require_once(plugin_dir_path(__FILE__).'components/enqueuing.php' );
require_once(plugin_dir_path(__FILE__).'components/settings.php' );
require_once(plugin_dir_path(__FILE__).'components/functions.php' );

// Admin welcome page
if (!function_exists('satori_deployments_main_page')) {
	function satori_deployments_main_page() {
	?>
	<div class="wrap">
	<h2>Satori deployments Plugins</h2>

	</div>
	<?php
	}
}

// Admin settings page
function satori_deployments_jenkins_settings_page() {
?>
<div class="wrap">
<h2>Satori deployments Jenkins Settings</h2>
<?php if (current_user_can('administrator')) { ?>
</div>
    <div class="satori-deployments-jenkins-button-container">
	    <div class="satori-deployments-jenkins-push-button">
	        <a id="satori-deployments-jenkins-staging-push" href="<?php echo wp_nonce_url( admin_url('admin-ajax.php?schedule=immediate'), 'process'); ?>"><button class="satori-deployments-jenkins-button">Push to Staging</button></a>
        </div>
<!--        <div class="satori-deployments-jenkins-box">-->
<!--        		  <select id="satori-deployments-jenkins-staging-push-schedule" name="satori_deployments_jenkins_staging_push_schedule">-->
<!--        		    <option value="immediate">Push Immediately</option>-->
<!--        		    <option value="tonight">Tonight</option>-->
<!--        		    <option value="tomorrow_morning">Tomorrow morning</option>-->
<!--        		    <option value="tomorrow_evening">Tomorrow evening</option>-->
<!--        		  </select>-->
<!--        </div>-->
        <div class="satori-deployments-jenkins-push-container">
            <div class="satori-deployments-jenkins-push-staging-progress"></div>
        </div>
    </div>
	<div class="satori-deployments-jenkins-button-container">
		<div class="satori-deployments-jenkins-push-button">
			<a id="satori-deployments-jenkins-push" href="<?php echo wp_nonce_url( admin_url('admin-ajax.php?action=satori_deployments_jenkins_push&schedule=immediate'), 'process'); ?>"><button class="satori-deployments-jenkins-button">Push to Production</button></a>
		</div>
<!--	<div class="satori-deployments-jenkins-box">-->
<!--		  <select id="satori-deployments-jenkins-push-schedule" name="satori_deployments_jenkins_push_schedule">-->
<!--		    <option value="immediate">Push Immediately</option>-->
<!--		    <option value="tonight">Tonight</option>-->
<!--            <option value="tomorrow_morning">Tomorrow morning</option>-->
<!--            <option value="tomorrow_evening">Tomorrow evening</option>-->
<!--		  </select>-->
<!--	</div>-->
		<div class="satori-deployments-jenkins-push-container">
			<div class="satori-deployments-jenkins-push-progress"></div>
		</div>
	</div>
	<div class="satori-deployments-jenkins-activity-log-container">
		<h3>Activity Log</h3>
	<div class="satori-deployments-jenkins-scrollabletextbox">
		<ul>
		<?php
		$activity_log_array = satori_deployments_jenkins_get_activity_log();
		if ($activity_log_array) {
			foreach ($activity_log_array as $activity_log) {
				echo '<li>' . $activity_log->user_name . ' ' . $activity_log->activity . ' on ' . $activity_log->activity_date . ', current status ' . $activity_log->job_status .'</li>';
			}
		}
		?>
		</ul>
	</div></div>
        <?php
        $current_user = wp_get_current_user();
        if (in_array($current_user->user_login, array('aminah', 'dawid', 'michal', 'ryan'))) {
        ?>

        <form method="post" action="options.php">
            <?php settings_fields( 'satori-deployments-jenkins-settings-group' ); ?>
            <?php do_settings_sections( 'satori-deployments-jenkins-settings-group' ); ?>
            <?php
            $locations = get_theme_mod( 'nav_menu_locations' );
            if (!empty($locations)) {
                foreach ($locations as $locationId => $menuValue) {
                    if (has_nav_menu($locationId)) {
                        $satori_deployments_jenkins_menu = $locationId;
                    }
                }
            }
            ?>
            <table class="form-table satori-deployments-jenkins-table">
                <tr valign="top">
                    <td><span id="satori-deployments-jenkins-notice">
    <?php
    settings_errors('satori_deployments_jenkins_url');
    settings_errors('satori_deployments_jenkins_staging_url');
    settings_errors('satori_deployments_jenkins_user');
    settings_errors('satori_deployments_jenkins_api');
    ?>
    </span></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Jenkins Build Trigger URL (Prod) : </th>
                    <td><input type="text" name="satori_deployments_jenkins_url" size="34" value="<?php echo (empty(esc_attr(get_option('satori_deployments_jenkins_url'))) ? '' : esc_attr(get_option('satori_deployments_jenkins_url'))); ?>"></td>
                </tr>
                <th scope="row">Jenkins Build Trigger URL (Staging) : </th>
                <td><input type="text" name="satori_deployments_jenkins_staging_url" size="34" value="<?php echo (empty(esc_attr(get_option('satori_deployments_jenkins_staging_url'))) ? '' : esc_attr(get_option('satori_deployments_jenkins_staging_url'))); ?>"></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Jenkins Build Username : </th>
                    <td><input type="text" name="satori_deployments_jenkins_user" size="34" value="<?php echo (empty(esc_attr(get_option('satori_deployments_jenkins_user'))) ? '' : esc_attr(get_option('satori_deployments_jenkins_user'))); ?>"></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Jenkins Build API Key : </th>
                    <td><input type="password" name="satori_deployments_jenkins_api" size="34" value="<?php echo (empty(esc_attr(get_option('satori_deployments_jenkins_api'))) ? '' : esc_attr(get_option('satori_deployments_jenkins_api'))); ?>"></td>
                </tr>
            </table>
            <?php submit_button(); ?>
        </form>
            <?php }
	} // is_admin
}


