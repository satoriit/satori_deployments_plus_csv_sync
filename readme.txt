=== Satori Deployments Jenkins Integration ===
* Contributors: originally based on Satori deployments Jenkins plugin rewrite for purposes of Satori IT
* Tags: jenkins, wordpress, wordpress automation, staging wordpress, staging, push, production push, jenkins push, wordpress deploy, wordpress build, build, deployment, deploy
* Requires at least: 3.0.1
* Tested up to: 5.7
* Stable tag: 1.0.0

Plugin that allows you to trigger a Jenkins hook straight from the Wordpress interface.

== Want to see the plugin in action? ==

You can view three example sites where this plugin is live :

- Example Site : [Wordpress Hosting](https://www.ectopic.org.uk "Wordpress Hosting")

== Features ==

- Settings area to allow you to define the Jenkins push URL including the authentication key

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the plugin files to the `/wp-content/plugins/satori-deployments-jenkins` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Navigate to the plugin settings page and define your settings

== Frequently Asked Questions ==

= I tested it on myself and its not working for me! =

You should monitor the Jenkins log to see if it is able to hit the site. Also monitor the server logs of your Wordpress site to identify if any problems (i.e. curl) are happening.

== Screenshots ==

1. Admin area

== Changelog ==

= 1.00 =
* Stable version created
